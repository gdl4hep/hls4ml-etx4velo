#!/bin/bash
# path to the ROOT repository
export ETX4VELO_HLS4ML_REPO="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export PYTHONPATH=$PYTHONPATH:$ETX4VELO_HLS4ML_REPO
export PYTHONPATH=$PYTHONPATH:$ETX4VELO_HLS4ML_REPO/setup
export PYTHONPATH=$PYTHONPATH:$ETX4VELO_HLS4ML_REPO/hls4ml
