# hls4ml-etx4velo

Inferring the ETX4VELO models on FPGAs, using [hls4ml](https://github.com/fastmachinelearning/hls4ml).

## Structure and contents

The various tasks/projects can be found in folder `projects`.

- `torch-mlp`: Synthesis of the ETX4VELO MLP in PyTorch, deployment on PYNQ-Z2 and validation
- `brevitas-mlp`: Quantization of the ETX4VELO MLP with Brevitas and synthesis with HLS
- `keras-from-torch`: Creation of a Keras model using the trained weights of the PyTorch ETX4VELO MLP, profiling and synthesis
- `latency-comparison`: Latency comparison between the inference of an MLP on FPGA vs GPU
- `qonnx-test`: Brevitas tutorial

## Setting up hls4ml on the LIP6 server

- Connect to a LIP6 server that has Vivado, e.g. `bip`, `bop`. 

	- From the LIP6 network (use `-X` flag to enable X11 forwarding).

	```
	ssh bop
	```
	
	- From outside.

	```
	ssh -J barder.lip6.fr bop
	```

- If you have not already installed miniforge3, install it.

```
curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
bash Miniforge3-$(uname)-$(uname -m).sh
```

- If you initialized miniforge3, source the bashrc file, to activate the base of miniforge3.

```
source .bashrc
```

- If you did not initialize it, source the activate executable in the bin folder.

```
source <miniforge3-install-path>/bin/activate
```

- Set up Vivado.

```
source /users/soft/xilinx/2019.1/Vivado/2019.1/settings64.sh
export LM_LICENSE_FILE=27009@house
```

## Choosing the environment to use

For running the hls4ml library

- from the conda installation: `hls4ml-etx4velo.yaml`, and

- from the local hls4ml repository: `hls4ml-etx4velo-local.yaml`, if you are using the branch `simplified` of ETX4VELO.

- from the local hls4ml repository: `hls4ml-etx4velo-local-updated.yaml`, if you are using the `main` branch of ETX4VELO.

- ~~to convert PyTorch ONNX model to TensorFlow: `hls4ml-etx4velo-deps-tf.yaml`.~~

## Installing and activating the environment

To install the chosen environment run the command below.

```
mamba env create -f env.yaml
```

Activate it.

```
conda activate env
```

## Setting up the PYTHONPATH for the repo

Source the setup script.

```
cd /some/path/hls4ml-etx4velo/
source setup/setup.sh
```

## Setting up ETX4VELO

Source the setup script.

```
cd /some/path/hls4ml-etx4velo/
source etx4velo/setup/setup.sh
```

## Launch jupyter


- Launch jupyter in the `hls4ml-etx4velo` repo.

```
jupyter notebook
```

- Forward the ports, to access the notebook.

	- From the LIP6 network.

	```
	ssh -L 8888:localhost:8888 bop
	```
	
	- From outside.
	
	```
	ssh -J barder.lip6.fr -L 8888:localhost:8888 bop
	```

## Summary

```
source .bashrc
cd hls4ml-etx4velo
source /users/soft/xilinx/2020.1/Vivado/2020.1/settings64.sh 
export LM_LICENSE_FILE=27009@house
source setup/setup.sh
source etx4velo/setup/setup.sh
conda activate hls4ml-etx4velo-local-updated
jupyter notebook --port 8889
```
	